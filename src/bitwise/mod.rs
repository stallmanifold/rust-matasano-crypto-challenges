pub mod base64;
pub mod bitwiseops;
pub mod hex_rep;

#[cfg(test)]
mod tests;
